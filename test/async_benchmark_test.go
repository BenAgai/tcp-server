// +build all async
package test

import (
	"testing"
	"time"
)

func TestAsyncServerSingleConnection10Seconds(t *testing.T) {
	NumOfDataTransmitters := uint(1)
	transmissionDuration := 10 * time.Second
	startTest(true, serverIP, serverPort, NumOfDataTransmitters, transmissionDuration, t)
}

func TestAsyncServerSingleConnection30Seconds(t *testing.T) {
	NumOfDataTransmitters := uint(1)
	transmissionDuration := 30 * time.Second
	startTest(true, serverIP, serverPort, NumOfDataTransmitters, transmissionDuration, t)
}

func TestAsyncServerSingleConnection2Minutes(t *testing.T) {
	NumOfDataTransmitters := uint(1)
	transmissionDuration := 2 * time.Minute
	startTest(true, serverIP, serverPort, NumOfDataTransmitters, transmissionDuration, t)
}

func TestAsyncServer5Connections10Seconds(t *testing.T) {
	NumOfDataTransmitters := uint(5)
	transmissionDuration := 10 * time.Second
	startTest(true, serverIP, serverPort, NumOfDataTransmitters, transmissionDuration, t)
}

func TestAsyncServer5Connections30Seconds(t *testing.T) {
	NumOfDataTransmitters := uint(5)
	transmissionDuration := 30 * time.Second
	startTest(true, serverIP, serverPort, NumOfDataTransmitters, transmissionDuration, t)
}

func TestAsyncServer5Connections2Minutes(t *testing.T) {
	NumOfDataTransmitters := uint(5)
	transmissionDuration := 2 * time.Minute
	startTest(true, serverIP, serverPort, NumOfDataTransmitters, transmissionDuration, t)
}

func TestAsyncServer10Connections10Seconds(t *testing.T) {
	NumOfDataTransmitters := uint(10)
	transmissionDuration := 10 * time.Second
	startTest(true, serverIP, serverPort, NumOfDataTransmitters, transmissionDuration, t)
}

func TestAsyncServer10Connections30Seconds(t *testing.T) {
	NumOfDataTransmitters := uint(10)
	transmissionDuration := 30 * time.Second
	startTest(true, serverIP, serverPort, NumOfDataTransmitters, transmissionDuration, t)
}

func TestAsyncServer10Connections2Minutes(t *testing.T) {
	NumOfDataTransmitters := uint(10)
	transmissionDuration := 2 * time.Minute
	startTest(true, serverIP, serverPort, NumOfDataTransmitters, transmissionDuration, t)
}
