package test

import (
	"fmt"
	"sync"
	"tcp_server/sync/examples"
	"tcp_server/sync/tcp/client"
	"testing"
	"time"

	asyncExamples "tcp_server/async/examples"
)

const (
	serverIP   = "127.0.0.1"
	serverPort = uint16(8080)
)

func startSyncServer(IP string, port uint16) (*examples.BytesCounterServer, error) {
	server, err := examples.NewBytesCounterServer()
	if err != nil {
		return nil, fmt.Errorf("Failed to create server: %s", err.Error())
	}

	err = server.Start(IP, port)
	if err != nil {
		return nil, fmt.Errorf("Failed to start server: %s", err.Error())
	}

	return server, nil
}

func startAsyncServer(IP string, port uint16) (*asyncExamples.BytesCounterServer, error) {
	server, err := asyncExamples.NewBytesCounterServer()
	if err != nil {
		return nil, fmt.Errorf("Failed to create server: %s", err.Error())
	}

	err = server.Start(IP, port)
	if err != nil {
		return nil, fmt.Errorf("Failed to start server: %s", err.Error())
	}

	return server, nil
}

func createAndConnectDataTransmitters(numOfTransmittersToCreate uint, serverIP string, serverPort uint16) ([]*client.DataTransmitter, error) {
	dataTransmitters := make([]*client.DataTransmitter, 0, numOfTransmittersToCreate)

	for i := uint(0); i < numOfTransmittersToCreate; i++ {
		dataTransmitter, err := client.NewDataTransmitter()
		if err != nil {
			return nil, fmt.Errorf("Failed to create data transmitter: %s", err.Error())
		}

		err = dataTransmitter.Connect(serverIP, serverPort)
		if err != nil {
			return nil, fmt.Errorf("Failed to connect data transmitter to server: %s", err.Error())
		}

		dataTransmitters = append(dataTransmitters, dataTransmitter)
	}

	return dataTransmitters, nil
}

func startDataTransmission(dataTransmitters []*client.DataTransmitter, dataTransmissionDuration time.Duration, t *testing.T) error {
	var wg sync.WaitGroup
	wg.Add(len(dataTransmitters))

	for _, dataTransmitter := range dataTransmitters {
		callback := func(dataTransmitter *client.DataTransmitter, wg *sync.WaitGroup) {
			defer wg.Done()
			err := dataTransmitter.StartDataTransmission(dataTransmissionDuration)
			if err != nil {
				t.Logf("Failed to start data tranmission on a data transmitter: %s", err.Error())
				t.FailNow()
			}
		}
		go callback(dataTransmitter, &wg)
	}

	wg.Wait()

	return nil
}

func closeDataTransmitters(dataTransmitters []*client.DataTransmitter) error {
	for _, dataTransmitter := range dataTransmitters {
		err := dataTransmitter.Close()
		if err != nil {
			return fmt.Errorf("Failed to close data transmitter: %s", err.Error())
		}
	}

	return nil
}

func startTest(isAsyncServer bool, serverIP string, serverPort uint16, NumOfDataTransmitters uint, transmissionDuration time.Duration, t *testing.T) {
	if !isAsyncServer {
		server, err := startSyncServer(serverIP, serverPort)
		if err != nil {
			t.Logf("Failed starting sync server: %s", err.Error())
			t.FailNow()
		}
		defer server.Stop()
	} else {
		server, err := startAsyncServer(serverIP, serverPort)
		if err != nil {
			t.Logf("Failed starting async server: %s", err.Error())
			t.FailNow()
		}
		defer server.Stop()
	}

	dataTransmitters, err := createAndConnectDataTransmitters(NumOfDataTransmitters, serverIP, serverPort)
	if err != nil {
		t.Logf("Failed preparing data transmitters: %s", err.Error())
		t.FailNow()
	}
	defer closeDataTransmitters(dataTransmitters)

	err = startDataTransmission(dataTransmitters, transmissionDuration, t)
	if err != nil {
		t.Logf("Failed starting data transmission: %s", err.Error())
		t.FailNow()
	}
}
