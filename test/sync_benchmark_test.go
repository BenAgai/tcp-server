// +build all sync
package test

import (
	"testing"
	"time"
)

func TestSyncServerSingleConnection10Seconds(t *testing.T) {
	NumOfDataTransmitters := uint(1)
	transmissionDuration := 10 * time.Second
	startTest(false, serverIP, serverPort, NumOfDataTransmitters, transmissionDuration, t)
}

func TestSyncServerSingleConnection30Seconds(t *testing.T) {
	NumOfDataTransmitters := uint(1)
	transmissionDuration := 30 * time.Second
	startTest(false, serverIP, serverPort, NumOfDataTransmitters, transmissionDuration, t)
}

func TestSyncServerSingleConnection2Minutes(t *testing.T) {
	NumOfDataTransmitters := uint(1)
	transmissionDuration := 2 * time.Minute
	startTest(false, serverIP, serverPort, NumOfDataTransmitters, transmissionDuration, t)
}

func TestSyncServer5Connections10Seconds(t *testing.T) {
	NumOfDataTransmitters := uint(5)
	transmissionDuration := 10 * time.Second
	startTest(false, serverIP, serverPort, NumOfDataTransmitters, transmissionDuration, t)
}

func TestSyncServer5Connections30Seconds(t *testing.T) {
	NumOfDataTransmitters := uint(5)
	transmissionDuration := 30 * time.Second
	startTest(false, serverIP, serverPort, NumOfDataTransmitters, transmissionDuration, t)
}

func TestSyncServer5Connections2Minutes(t *testing.T) {
	NumOfDataTransmitters := uint(5)
	transmissionDuration := 2 * time.Minute
	startTest(false, serverIP, serverPort, NumOfDataTransmitters, transmissionDuration, t)
}

func TestSyncServer10Connections10Seconds(t *testing.T) {
	NumOfDataTransmitters := uint(10)
	transmissionDuration := 10 * time.Second
	startTest(false, serverIP, serverPort, NumOfDataTransmitters, transmissionDuration, t)
}

func TestSyncServer10Connections30Seconds(t *testing.T) {
	NumOfDataTransmitters := uint(10)
	transmissionDuration := 30 * time.Second
	startTest(false, serverIP, serverPort, NumOfDataTransmitters, transmissionDuration, t)
}

func TestSyncServer10Connections2Minutes(t *testing.T) {
	NumOfDataTransmitters := uint(5)
	transmissionDuration := 2 * time.Minute
	startTest(false, serverIP, serverPort, NumOfDataTransmitters, transmissionDuration, t)
}
