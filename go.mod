module tcp_server

go 1.15

require (
	github.com/panjf2000/gnet v1.3.2
	github.com/satori/go.uuid v1.2.0
	github.com/sirupsen/logrus v1.7.0
)
