# TCP-server

## How to run tests

* In order to run only the sync tests type the following command (from the test directory):

go test -v -run TestSync

* In order to run only the async tests type the following command (from the test directory):

go test -v -run TestAsync