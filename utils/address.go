package utils

import (
	"net"
	"strconv"
)

func ConstructAddressString(ip string, port uint16) string {
	portStr := strconv.Itoa(int(port))
	address := net.JoinHostPort(ip, portStr)

	return address
}
