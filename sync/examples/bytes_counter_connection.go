package examples

import (
	"errors"
	"fmt"
	"net"

	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"
)

type BytesCounterConnection struct {
	innerConn          net.Conn
	totalBytesReceived uint
	innerUUID          uuid.UUID
}

// Public functions

func NewBytesCounterConnection(innerConn net.Conn) (*BytesCounterConnection, error) {
	if innerConn == nil {
		return nil, errors.New("nil innerConn is not allowed")
	}

	conn := &BytesCounterConnection{
		innerConn: innerConn,
	}

	return conn, nil
}

func (conn *BytesCounterConnection) Start() error {
	go conn.countBytes()

	return nil
}

//  Private functions

func (conn *BytesCounterConnection) countBytes() {
	// Make a buffer to hold incoming data.
	buf := make([]byte, 1024)
	for {
		// Read the incoming data into the buffer.
		dataLen, err := conn.innerConn.Read(buf)
		if err != nil {
			msg := fmt.Sprintf("[%s] Failed reading: %s", conn.innerUUID.String(), err.Error())
			log.Error(msg)
			break
		}

		conn.totalBytesReceived += uint(dataLen)
	}

	// Close the connection when you're done with it.
	conn.innerConn.Close()

	msg := fmt.Sprintf("[%s] session received %d bytes", conn.innerUUID.String(), conn.totalBytesReceived)
	log.Info(msg)
}
