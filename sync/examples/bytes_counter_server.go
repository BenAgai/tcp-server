package examples

import (
	"fmt"
	"net"
	"tcp_server/utils"

	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"
)

const (
	UUIDSizeInBytes = 16 // 128 bits
)

type BytesCounterServer struct {
	address        string
	listener       net.Listener
	listenerClosed bool
}

// Public functions

func NewBytesCounterServer() (*BytesCounterServer, error) {
	return &BytesCounterServer{}, nil
}

func (server *BytesCounterServer) Start(IP string, port uint16) error {
	address := utils.ConstructAddressString(IP, port)
	listener, err := net.Listen("tcp", address)
	if err != nil {
		return fmt.Errorf("Failed listening: %s", err.Error())
	}

	server.address = address
	server.listener = listener

	go server.waitForConnections()

	return nil
}

func (server *BytesCounterServer) Stop() error {
	if server.listenerClosed {
		return nil
	}

	err := server.listener.Close()
	if err != nil {
		return fmt.Errorf("Failed closing listener: %s", err.Error())
	}
	server.listenerClosed = true

	return nil
}

// Private functions

func (server *BytesCounterServer) waitForConnections() {
	msg := fmt.Sprintf("Listening on %s", server.address)
	log.Info(msg)
	for {
		// Listen for an incoming connection.
		conn, err := server.listener.Accept()
		if err != nil {
			msg := fmt.Sprintf("Failed on accept: %s. Listening stopped.", err.Error())
			log.Error(msg)
			server.listener.Close()
			return
		}

		// Handle connections in a new goroutine.
		go server.handleRequest(conn)
	}
}

func (server *BytesCounterServer) handleRequest(conn net.Conn) {
	bytesCounterConnection, err := NewBytesCounterConnection(conn)
	if err != nil {
		msg := fmt.Sprintf("Failed creagin BytesCounterConnection: %s", err.Error())
		log.Error(msg)
		conn.Close()
		return
	}

	// Make a buffer to hold incoming ID.
	buf := make([]byte, UUIDSizeInBytes)

	// Read the incoming data into the buffer.
	dataLen, err := conn.Read(buf)
	if err != nil {
		msg := fmt.Sprintf("Failed reading connection ID: %s", err.Error())
		log.Error(msg)
		conn.Close()
		return
	}

	if dataLen != UUIDSizeInBytes {
		log.Error("Too small ID was received for connection")
		conn.Close()
		return
	}

	parsedUUID, err := uuid.FromBytes(buf)
	if err != nil {
		msg := fmt.Sprintf("Failed parsing UUID from async conn: %s", err.Error())
		log.Error(msg)
		conn.Close()
		return
	}
	bytesCounterConnection.innerUUID = parsedUUID

	bytesCounterConnection.Start()
}
