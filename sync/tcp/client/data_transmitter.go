package client

import (
	"errors"
	"fmt"
	"net"
	"strings"
	"tcp_server/utils"
	"time"

	uuid "github.com/satori/go.uuid"

	log "github.com/sirupsen/logrus"
)

const (
	dataSize = 1000 // arbitrary chosen value
)

type DataTransmitter struct {
	conn *net.TCPConn
}

// Public functions

func NewDataTransmitter() (*DataTransmitter, error) {
	return &DataTransmitter{}, nil
}

func (transmitter *DataTransmitter) Connect(serverIP string, serverPort uint16) error {
	address := utils.ConstructAddressString(serverIP, serverPort)
	tcpAddr, err := net.ResolveTCPAddr("tcp", address)
	if err != nil {
		return fmt.Errorf("Failed at ResolveTCPAddr: %s", err.Error())
	}

	conn, err := net.DialTCP("tcp", nil, tcpAddr)
	if err != nil {
		return fmt.Errorf("Failed at dial: %s", err.Error())
	}
	transmitter.conn = conn

	// Sends the expected UUID as the client's ID
	id := uuid.NewV1().String()
	writtenBytes, err := transmitter.conn.Write([]byte(id))
	if err != nil {
		conn.Close()
		return fmt.Errorf("Failed writing ID to conn: %s", err.Error())
	}

	if writtenBytes < len(id) {
		conn.Close()
		return errors.New("Failed at writing entire ID to conn")
	}

	transmitter.conn = conn

	return nil
}

func (transmitter *DataTransmitter) StartDataTransmission(dataTransmissionDuration time.Duration) error {
	ticker := time.NewTicker(dataTransmissionDuration)

	dataToWrite := strings.Repeat("#", dataSize)
	for {
		select {
		case <-ticker.C:
			log.Info("Data transmission duration passed, exiting..")
			return nil
		default:
			writtenBytes, err := transmitter.conn.Write([]byte(dataToWrite))
			if err != nil {
				return fmt.Errorf("Failed at writing to conn: %s", err.Error())
			}

			if writtenBytes < len(dataToWrite) {
				return errors.New("Failed at writing entire buffer to conn")
			}
		}
	}
}

func (transmitter *DataTransmitter) Close() error {
	if transmitter.conn == nil {
		return nil
	}

	err := transmitter.conn.Close()
	if err != nil {
		return fmt.Errorf("Failed at closing conn: %s", err.Error())
	}

	transmitter.conn = nil

	return nil
}
