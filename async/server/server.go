package server

type Server interface {
	Listen(IP string, port uint16) error // Also good for cases where we want to change the listen address after calling stop(..)
	Start() error
	Stop() error

	RegisterForEvents(eventHandler ServerEventHandler, userArgs interface{}) error
}
