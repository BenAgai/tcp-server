package server

import (
	"tcp_server/async/connection"
)

type ServerEventHandler interface {
	OnNewConnection(conn connection.Connection)
}