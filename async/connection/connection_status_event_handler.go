package connection

type ConnectionStatusEventHandler interface {
	OnConnectionClosed(conn Connection, connLastErr error, userArgs interface{})
}