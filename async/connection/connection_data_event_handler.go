package connection

type OnReceivedDataResult uint

const (
	EventHandlingFinished OnReceivedDataResult = iota + 1
	
	/*
		This will cause the same event to be triggered again.
		Useful if users wants to process the available data in chunks, for example if the available data contains multiple applicative messages.
		I don't want to force the users to consume all available data when handling the event.
	*/
	EventHandlingNotFinished
)

type ConnectionDataEventHandler interface {
	OnReceivedData(conn Connection, totalBytesAvailableForRead uint, userArgs interface{}) (OnReceivedDataResult)
}