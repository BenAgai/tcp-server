package connection

import (
	"net"
)

type Connection interface {
	LocalAddr() (addr net.Addr)  // LocalAddr is the connection's local socket address.
	RemoteAddr() (addr net.Addr) // RemoteAddr is the connection's remote peer address.

	Write(buffer []byte) error
	ReadAllAvailableData() (buffer []byte, bufferSize uint) // For cases where user don't want to read in chunks, but wants to consume everything in one call.
	ReadN(n uint) (buffer []byte, bufferSize uint)

	Close() error

	RegisterForDataEvents(eventHandler ConnectionDataEventHandler, userArgs interface{}) error
	UnRegisterForDataEvents(eventHandler ConnectionDataEventHandler) error

	RegisterForStatusEvents(eventHandler ConnectionStatusEventHandler, userArgs interface{}) error
}
