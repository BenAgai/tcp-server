package examples

import (
	"errors"
	"fmt"
	"sync"

	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"

	"tcp_server/async/connection"
	"tcp_server/async/tcp"
)

const (
	UUIDSizeInBytes = 16 // 128 bits
)

type BytesCounterServer struct {
	innerServer  *tcp.Server
	connPool     []*BytesCounterConnection
	connPoolLock sync.RWMutex
}

// Public functions

func NewBytesCounterServer() (*BytesCounterServer, error) {
	var innerServerConf tcp.ServerConfiguration
	innerServer, err := tcp.NewServer(&innerServerConf)
	if err != nil {
		return nil, fmt.Errorf("Failed creating Async TCP server: %s", err.Error())
	}

	server := &BytesCounterServer{
		innerServer: innerServer,
		connPool:    make([]*BytesCounterConnection, 0),
	}

	return server, nil
}

func (server *BytesCounterServer) Start(IP string, port uint16) error {
	err := server.innerServer.RegisterForEvents(server, nil)
	if err != nil {
		return fmt.Errorf("Failed registering for data events of Async TCP server: %s", err.Error())
	}

	err = server.innerServer.Listen(IP, port)
	if err != nil {
		return fmt.Errorf("Failed at listen of Async TCP server: %s", err.Error())
	}

	startCallback := func() {
		err = server.innerServer.Start()
		if err != nil {
			msg := fmt.Sprintf("Failed starting of Async TCP server: %s", err.Error())
			log.Error(msg)
		}
	}
	go startCallback()

	return nil
}

func (server *BytesCounterServer) Stop() error {
	err := server.innerServer.Stop()
	if err != nil {
		return fmt.Errorf("Failed stopping of Async TCP server: %s", err.Error())
	}

	return nil
}

func (server *BytesCounterServer) OnNewConnection(conn connection.Connection) {
	tcpConn, ok := conn.(*tcp.Connection)
	if !ok {
		log.Error("Bad type of connection was passed on connection notification")
		return
	}

	newConnection, err := NewBytesCounterConnection(tcpConn)
	if err != nil {
		msg := fmt.Sprintf("Failed creating BytesCounterConnection: %s", err.Error())
		log.Error(msg)
		return
	}

	err = server.addConnToPool(newConnection)
	if err != nil {
		msg := fmt.Sprintf("Failed adding connection to pool: %s", err.Error())
		log.Error(msg)
		return
	}

	err = conn.RegisterForDataEvents(server, nil)
	if err != nil {
		msg := fmt.Sprintf("Failed registering server for connection data events: %s", err.Error())
		log.Error(msg)
		return
	}

	// Register our new connection to handle status events of the underlying connection
	err = conn.RegisterForStatusEvents(newConnection, nil)
	if err != nil {
		msg := fmt.Sprintf("Failed registering server for connection status events: %s", err.Error())
		log.Error(msg)
		return
	}
}

func (server *BytesCounterServer) OnReceivedData(asynConn connection.Connection, totalBytesAvailableForRead uint, userArgs interface{}) connection.OnReceivedDataResult {
	if totalBytesAvailableForRead < UUIDSizeInBytes {
		return connection.EventHandlingFinished
	}

	if totalBytesAvailableForRead >= UUIDSizeInBytes {
		bytesCounterConn, err := server.findConnInPool(asynConn)
		if err != nil {
			msg := fmt.Sprintf("Failed finding BytesCounterConnection for given asynConn: %s", err.Error())
			log.Error(msg)
			asynConn.Close()

			return connection.EventHandlingFinished
		}

		err = parseAndSetIDForBytesCounterConnection(bytesCounterConn, asynConn)
		if err != nil {
			msg := fmt.Sprintf("Failed finding parsing ID for BytesCounterConnection: %s", err.Error())
			log.Error(msg)
			asynConn.Close()

			err = server.removeConnFromPool(bytesCounterConn)
			if err != nil {
				msg := fmt.Sprintf("Failed removing BytesCounterConnection after parsing ID error: %s", err.Error())
				log.Error(msg)
				return connection.EventHandlingFinished
			}

			return connection.EventHandlingFinished
		}

		err = server.registerConnectionToHandleDataEventsInsteadOfServer(bytesCounterConn, asynConn)
		if err != nil {
			msg := fmt.Sprintf("Failed registering BytesCounterConnection to handle data events of asynConn: %s", err.Error())
			log.Error(msg)
			asynConn.Close()

			err = server.removeConnFromPool(bytesCounterConn)
			if err != nil {
				msg := fmt.Sprintf("Failed removing BytesCounterConnection after BytesCounterConnection registration for data events error: %s", err.Error())
				log.Error(msg)
				return connection.EventHandlingFinished
			}

			return connection.EventHandlingFinished
		}

		/*
			I will return EventHandlingNotFinished instead of EventHandlingFinished so the bytes_counter_connection would be notified
			on remaining data (if any exists).
		*/
		return connection.EventHandlingNotFinished
	}

	return connection.EventHandlingFinished
}

// Private functions

func (server *BytesCounterServer) registerConnectionToHandleDataEventsInsteadOfServer(bytesCounterConn *BytesCounterConnection,
	asynConn connection.Connection) error {

	/*
		Un-register from data events - this can cause an issue. If there is data in the buffer but no new data arrives
		after I un-register the user (currently the bytes_counter_connection itself) will not be notified on future data events.
		In order to solve this issue - I will register the bytes_counter_connection for the connection data events,
	*/
	err := asynConn.UnRegisterForDataEvents(server)
	if err != nil {
		return fmt.Errorf("Failed unregistering from async conn data events: %s", err.Error())
	}

	err = asynConn.RegisterForDataEvents(bytesCounterConn, nil)
	if err != nil {
		return fmt.Errorf("Failed registering bytes counter connection to handle async conn data events: %s", err.Error())
	}

	return nil
}

func (server *BytesCounterServer) addConnToPool(conn *BytesCounterConnection) error {
	server.connPoolLock.Lock()
	defer server.connPoolLock.Unlock()

	server.connPool = append(server.connPool, conn)

	return nil
}

func (server *BytesCounterServer) removeConnFromPool(conn *BytesCounterConnection) error {
	server.connPoolLock.Lock()
	defer server.connPoolLock.Unlock()

	isConnFound := false
	indexOfConnToBeRemoved := 0
	for index, connInPool := range server.connPool {
		if connInPool == conn {
			isConnFound = true
			indexOfConnToBeRemoved = index
			break
		}
	}

	if !isConnFound {
		return errors.New("No such BytesCounterConnection was found in pool for removal")
	}

	// Replace last conn with the one to be deleted and delete the last element
	lastConnIndex := len(server.connPool) - 1
	lastConn := server.connPool[lastConnIndex]
	server.connPool[indexOfConnToBeRemoved] = lastConn
	server.connPool = server.connPool[:lastConnIndex]

	return nil
}

func (server *BytesCounterServer) findConnInPool(c connection.Connection) (*BytesCounterConnection, error) {
	server.connPoolLock.RLock()
	defer server.connPoolLock.RUnlock()

	for _, conn := range server.connPool {
		if conn.innerConn == c {
			return conn, nil
		}
	}

	return nil, errors.New("No BytesCounterConnection was found")
}

// Private static functions

func parseAndSetIDForBytesCounterConnection(bytesCounterConn *BytesCounterConnection,
	asynConn connection.Connection) error {

	rawUUID, _ := asynConn.ReadN(UUIDSizeInBytes)
	parsedUUID, err := uuid.FromBytes(rawUUID)
	if err != nil {
		return fmt.Errorf("Failed parsing UUID from async conn: %s", err.Error())
	}

	// Set UUID for our conn
	bytesCounterConn.innerUUID = parsedUUID

	return nil
}
