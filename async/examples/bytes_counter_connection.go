package examples

import (
	"errors"
	"fmt"

	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"

	"tcp_server/async/connection"
	"tcp_server/async/tcp"
)

type BytesCounterConnection struct {
	innerConn          *tcp.Connection
	totalBytesReceived uint
	innerUUID          uuid.UUID
}

// Public functions

func NewBytesCounterConnection(innerConn *tcp.Connection) (*BytesCounterConnection, error) {
	if innerConn == nil {
		return nil, errors.New("nil innerConn is not allowed")
	}

	conn := &BytesCounterConnection{
		innerConn: innerConn,
	}

	return conn, nil
}

func (conn *BytesCounterConnection) OnReceivedData(asynConn connection.Connection, totalBytesAvailableForRead uint, userArgs interface{}) connection.OnReceivedDataResult {
	conn.totalBytesReceived += totalBytesAvailableForRead
	asynConn.ReadAllAvailableData()

	return connection.EventHandlingFinished
}

func (conn *BytesCounterConnection) OnConnectionClosed(asynConn connection.Connection, connLastErr error, userArgs interface{}) {
	msg := fmt.Sprintf("[%s] session received %d bytes", conn.innerUUID.String(), conn.totalBytesReceived)
	log.Info(msg)
}
