package tcp

import (
	"context"
	"errors"
	"fmt"
	"net"
	"strconv"
	"sync"
	"time"

	"github.com/panjf2000/gnet"
	log "github.com/sirupsen/logrus"

	"tcp_server/async/server"
)

const (
	innerServerTCPAddressFormat = "tcp://%s"
)

type ServerConfiguration struct {
	eventHandler server.ServerEventHandler
	ip           string
	port         uint16
}

type Server struct {
	conf               *ServerConfiguration
	innerServer        gnet.EventServer // Used only for default handling for part of the notified events
	innerServerAddress string
	connPool           []*Connection
	connPoolLock       sync.RWMutex
}

//  Public functions

func NewServer(conf *ServerConfiguration) (*Server, error) {
	if conf == nil {
		return nil, errors.New("nil conf is not allowed")
	}

	server := &Server{
		conf:     conf,
		connPool: make([]*Connection, 0),
	}

	return server, nil
}

func (server *Server) Listen(IP string, port uint16) error {
	if IP == "" || len(IP) == 0 {
		return errors.New("Empty IP is not allowed")
	}

	server.conf.ip = IP
	server.conf.port = port

	return nil
}

func (server *Server) Start() error {
	if server.conf.ip == "" || len(server.conf.ip) == 0 {
		return errors.New("IP is not set")
	}

	if server.conf.eventHandler == nil {
		return errors.New("eventHandler is not set - make sure you register for events before starting the server")
	}

	server.innerServerAddress = constructTCPAddressForInnerServer(server.conf.ip, server.conf.port)

	return gnet.Serve(server, server.innerServerAddress, setServerOptions)
}

func (server *Server) Stop() error {
	if server.innerServerAddress == "" || len(server.innerServerAddress) == 0 {
		return errors.New("Server was not started before calling stop")
	}

	return gnet.Stop(context.Background(), server.innerServerAddress)
}

func (server *Server) RegisterForEvents(eventHandler server.ServerEventHandler, userArgs interface{}) error {
	if eventHandler == nil {
		return errors.New("nil eventHandler is not allowed")
	}

	server.conf.eventHandler = eventHandler

	return nil
}

func (server *Server) OnInitComplete(gnetServer gnet.Server) (action gnet.Action) {
	return server.innerServer.OnInitComplete(gnetServer)
}

func (server *Server) OnShutdown(gnetServer gnet.Server) {
	server.innerServer.OnShutdown(gnetServer)
}

func (server *Server) OnOpened(c gnet.Conn) (out []byte, action gnet.Action) {
	newConnection, err := newConnection(c)
	if err != nil {
		return nil, gnet.Close
	}

	err = server.addConnToPool(newConnection)
	if err != nil {
		return nil, gnet.Close
	}

	server.conf.eventHandler.OnNewConnection(newConnection)

	return nil, gnet.None
}

func (server *Server) OnClosed(c gnet.Conn, lastErr error) (action gnet.Action) {
	Conn, err := server.findConnInPool(c)
	if err != nil {
		return gnet.None
	}

	err = server.removeConnFromPool(Conn)
	if err != nil {
		msg := fmt.Sprintf("Failed removing connection from pool after connection closure: %s", err.Error())
		log.Error(msg)
	}

	Conn.notifyOnConnectionClosed(lastErr)

	return gnet.None
}

func (server *Server) PreWrite() {
	server.innerServer.PreWrite()
}

func (server *Server) React(frame []byte, c gnet.Conn) (out []byte, action gnet.Action) {
	Conn, err := server.findConnInPool(c)
	if err != nil {
		return nil, gnet.Close // Unknown connection - close it
	}

	Conn.notifyOnReceivedDataAndHandleAction(frame)

	return nil, gnet.None
}

func (server *Server) Tick() (delay time.Duration, action gnet.Action) {
	return server.innerServer.Tick()
}

// Private functions

func (server *Server) addConnToPool(conn *Connection) error {
	server.connPoolLock.Lock()
	defer server.connPoolLock.Unlock()

	server.connPool = append(server.connPool, conn)

	return nil
}

func (server *Server) removeConnFromPool(conn *Connection) error {
	server.connPoolLock.Lock()
	defer server.connPoolLock.Unlock()

	isConnFound := false
	indexOfConnToBeRemoved := 0
	for index, connInPool := range server.connPool {
		if connInPool == conn {
			isConnFound = true
			indexOfConnToBeRemoved = index
			break
		}
	}

	if !isConnFound {
		return errors.New("No such connection was found in pool for removal")
	}

	// Replace last conn with the one to be deleted and delete the last element
	lastConnIndex := len(server.connPool) - 1
	lastConn := server.connPool[lastConnIndex]
	server.connPool[indexOfConnToBeRemoved] = lastConn
	server.connPool = server.connPool[:lastConnIndex]

	return nil
}

func (server *Server) findConnInPool(c gnet.Conn) (*Connection, error) {
	server.connPoolLock.RLock()
	defer server.connPoolLock.RUnlock()

	for _, conn := range server.connPool {
		if conn.innerConn == c {
			return conn, nil
		}
	}

	return nil, errors.New("No connection was found")
}

// Private static functions

func setServerOptions(innerServerOptions *gnet.Options) {
	innerServerOptions.Multicore = true
	innerServerOptions.LockOSThread = true
	innerServerOptions.ReusePort = true
}

func constructTCPAddressForInnerServer(ip string, port uint16) string {
	portStr := strconv.Itoa(int(port))
	address := net.JoinHostPort(ip, portStr)

	innerServerAddress := fmt.Sprintf(innerServerTCPAddressFormat, address)

	return innerServerAddress
}
