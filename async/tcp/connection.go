package tcp

import (
	"errors"
	"fmt"
	"net"
	"sync"

	"github.com/panjf2000/gnet"
	log "github.com/sirupsen/logrus"

	"tcp_server/async/connection"
)

type dataEventsObserver struct {
	eventHandler connection.ConnectionDataEventHandler
	userArgs     interface{}
}

type statusEventsObserver struct {
	eventHandler connection.ConnectionStatusEventHandler
	userArgs     interface{}
}

type state uint

const (
	maxDataBufferSize = 1024 * 65 // 65K - Arbitrary chosen value

	Open state = iota + 1
	Closed
)

type Connection struct {
	innerBuffer     []byte
	innerBufferLock sync.RWMutex

	innerConn gnet.Conn
	state     state

	dataEventObserver     *dataEventsObserver
	dataEventObserverLock sync.RWMutex

	statusEventObservers     []*statusEventsObserver
	statusEventObserversLock sync.RWMutex
}

// Public functions

func (conn *Connection) LocalAddr() (addr net.Addr) {
	return conn.innerConn.LocalAddr()
}

func (conn *Connection) RemoteAddr() (addr net.Addr) {
	return conn.innerConn.RemoteAddr()
}

func (conn *Connection) Write(buffer []byte) error {
	return conn.innerConn.AsyncWrite(buffer)
}

func (conn *Connection) ReadAllAvailableData() ([]byte, uint) {
	conn.innerBufferLock.RLock()
	buffer := conn.innerBuffer
	conn.innerBuffer = make([]byte, 0, maxDataBufferSize)
	conn.innerBufferLock.RUnlock()

	bufferSize := uint(len(buffer))

	return buffer, bufferSize
}

func (conn *Connection) ReadN(n uint) ([]byte, uint) {
	conn.innerBufferLock.RLock()
	buffer := conn.innerBuffer[0:n]
	conn.innerBuffer = conn.innerBuffer[n:]
	conn.innerBufferLock.RUnlock()

	bufferSize := uint(len(buffer))

	return buffer, bufferSize
}

func (conn *Connection) Close() error {
	if conn.state == Closed {
		return nil
	}

	err := conn.innerConn.Close()
	if err != nil {
		return err
	}
	conn.state = Closed

	conn.notifyOnConnectionClosed(nil)

	return nil
}

func (conn *Connection) RegisterForDataEvents(eventHandler connection.ConnectionDataEventHandler, userArgs interface{}) error {
	if eventHandler == nil {
		return errors.New("nil eventHandler is not allowed")
	}

	// There is no reason for multiple observers since one of them can handle the event in a way (for example reading data)
	// that makes the notification info un-reliable for the rest of the observers.
	if conn.dataEventObserver != nil {
		return errors.New("Only single observer can be registered for data events")
	}

	var observer dataEventsObserver
	observer.eventHandler = eventHandler
	observer.userArgs = userArgs

	conn.dataEventObserverLock.Lock()
	defer conn.dataEventObserverLock.Unlock()

	conn.dataEventObserver = &observer

	return nil
}

func (conn *Connection) UnRegisterForDataEvents(eventHandler connection.ConnectionDataEventHandler) error {
	if eventHandler == nil {
		return errors.New("nil eventHandler is not allowed")
	}

	if conn.dataEventObserver == nil {
		return errors.New("No event handler is registered")
	}

	conn.dataEventObserverLock.Lock()
	defer conn.dataEventObserverLock.Unlock()

	conn.dataEventObserver = nil

	return nil
}

func (conn *Connection) RegisterForStatusEvents(eventHandler connection.ConnectionStatusEventHandler, userArgs interface{}) error {
	if eventHandler == nil {
		return errors.New("nil eventHandler is not allowed")
	}

	var observer statusEventsObserver
	observer.eventHandler = eventHandler
	observer.userArgs = userArgs

	conn.statusEventObserversLock.Lock()
	defer conn.statusEventObserversLock.Unlock()

	conn.statusEventObservers = append(conn.statusEventObservers, &observer)

	return nil
}

// Private functions

func newConnection(innerConn gnet.Conn) (*Connection, error) {
	if innerConn == nil {
		return nil, errors.New("nil innerConn is not allowed")
	}

	conn := &Connection{
		innerBuffer:          make([]byte, 0, maxDataBufferSize),
		innerConn:            innerConn,
		state:                Open,
		statusEventObservers: make([]*statusEventsObserver, 0),
	}

	return conn, nil
}

func (conn *Connection) notifyOnReceivedDataAndHandleAction(data []byte) {
	conn.innerBufferLock.Lock()
	conn.innerBuffer = append(conn.innerBuffer, data...)
	conn.innerBufferLock.Unlock()

	if conn.dataEventObserver == nil {
		return
	}

	observer := conn.dataEventObserver
	result := observer.eventHandler.OnReceivedData(conn, uint(len(data)), observer.userArgs)

	if result == connection.EventHandlingNotFinished {
		err := conn.innerConn.Wake()
		if err != nil {
			msg := fmt.Sprintf("Failed at waking inner connection: %s", err.Error())
			log.Error(msg)
		}
	}
}

func (conn *Connection) notifyOnConnectionClosed(lastError error) {
	for _, observer := range conn.statusEventObservers {
		observer.eventHandler.OnConnectionClosed(conn, lastError, observer.userArgs)
	}
}
